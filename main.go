package main

import (
	"log"
	"time"
)

const (
	CONFIG_FILE = "./config.yml"
)

var (
	config  = load_config(CONFIG_FILE)
	alerted = make(map[string]time.Time)
)

func run(coin string, currency string) {
	// Check if pair was alerted in the last X hours.
	if _, exist := alerted[coin]; exist {
		return
	}

	// Get percentage
	percentage := get_percentage(coin, currency)
	if percentage < config.Percentage {
		// Exit if percentage is not bigger (TARGET)
		return
	}

	// PUMP
	// Alert
	log.Printf("Alert: %s %d\n", coin, percentage)
	send_email(coin)

	// Save alert
	alerted[coin] = time.Now()
}

func main() {
	for {
		for _, currency := range config.Currencies {
			// Get currency name + coins of currency
			for currency_name, coins := range currency {
				for _, coin := range coins {
					run(coin, currency_name)
				}
			}
		}

		// Wait for next check
		time.Sleep(config.Sleep * time.Minute)

		// Reset alerted pairs
		for key, last_alert := range alerted {
			if time.Since(last_alert) > config.Reset*time.Hour {
				// If have passed X hours from last alert.
				log.Println("Reset: ", key)
				delete(alerted, key)
			}
		}
	}
}
