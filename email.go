package main

import (
	"log"
	"net/smtp"
	"strings"
)

func send_email(pair string) {
	// Build message
	// From:
	msg := "From: " + config.From.Email + "\n"
	// To:
	msg += "To: " + strings.Join(config.To, ",") + "\n"
	// Subject
	msg += "Subject: Go see -> " + pair + "\n\n"

	// Send email
	err := smtp.SendMail(config.From.Smtp_addr,
		smtp.PlainAuth("", config.From.Email, config.From.Password, config.From.Smtp_host),
		config.From.Email,
		config.To,
		[]byte(msg),
	)

	if err != nil {
		log.Printf("smtp error: %s", err)
	}
}
