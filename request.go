package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// Example: GET https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=btc&include_24hr_change=true
type Simple_price struct {
	Price     float64
	Change_24 float64
}

func get_price(coin string, currency string) map[string]interface{} {
	var response *http.Response
	var err error

	// fmt.Println("https://api.coingecko.com/api/v3/simple/price?ids=" + coin + "&vs_currencies=" + currency + "&include_24hr_change=true")

	for { //While timeout == true
		response, err = http.Get(
			"https://api.coingecko.com/api/v3/simple/price?ids=" + coin + "&vs_currencies=" + currency + "&include_24hr_change=true",
		)
		if err != nil {
			log.Println("Request:", err)
			return nil
		}

		// If the status code is not 429, break the loop, no timeout
		if response.StatusCode != http.StatusTooManyRequests {
			break
		}

		// log.Println("Timeout")

		// If the status code is 429, sleep for a while before retrying
		time.Sleep(time.Minute * 1)
	}

	if response.StatusCode != http.StatusOK {
		log.Println("Failed: ", coin, currency)
		return nil
	}

	var json_values map[string]interface{}
	if err = json.NewDecoder(response.Body).Decode(&json_values); err != nil {
		return nil
	}

	return json_values
}

func get_percentage(coin string, currency string) int {
	json_values := get_price(coin, currency)
	if len(json_values) == 0 {
		log.Println("Fail getting data:", coin, currency)
		return 0
	}

	var sp Simple_price
	field_change := currency + "_24h_change"
	for _, value := range json_values {
		// Convert interface to map[string]interface{}
		if fields, ok := value.(map[string]interface{}); ok {
			// Convert to structure
			sp = Simple_price{
				Price:     fields[currency].(float64),
				Change_24: fields[field_change].(float64),
			}
		}
	}

	return int(sp.Change_24)
}
